# Lab 5.1 Volley & REST, users, albums, photos - OPPDATERT 06.02.2020

I denne oppgaven skal du bruke JSON-ressurser/testdata fra denne siten https://jsonplaceholder.typicode.com (Lenker til en ekstern side.) Alternativt kan du relativt enkelt implementere din egen «fake» REST-server, som vist her https://my-json-server.typicode.com (Lenker til en ekstern side.).

Uansett, appen skal bruke følgende ressurser:

- **/users**
- **/albums**
- **/photos**

Appen starter med å vise en liste med brukere (fra **/users**). Disse presenteres i en liste, i en egen Aktivitet. Ved klikk på en av brukerne skal brukerens album (/albums) vises i en annen Aktivitet  (dvs. du må hente alle album for valgt bruker). Ved klikk på album vises id og navn på alle bilder (og ikke nødvendigvis med thumbnail-bilder) i en tredje Aktivitet (et fragment som dekker hele skjermen - UTGÅR). Klikk på thumbnail viser bildet i full størrelse i en fjerde aktivitet.

Endringene er i hovedsak at du bruker flere aktiviteter i stedet for en aktivitet med flere fragmenter. Det betyr også at du ikke nødvendigvis trenger å bruke fragmenter i det hele tatt. Det viser seg også at url-ene som ligger i photo-objektene, både thumbnailUrl og url, ikke fungerer som forventet når du forsøker å laste dem ned vha. Volley. Du kan derfor bruke et hardkodet bilde for url (og evt. thumbnailUrl). Bruk f.eks. https://kark.uit.no/~wfa004/3d/three/del8/images/snowflake2.png (Lenker til en ekstern side.) som thumbnail og https://kark.uit.no/~wfa004/3d/three/del8/images/water1.png (Lenker til en ekstern side.) som bilde-url (eller hvilken som helst annen url).

Noen tips:

Lag en ViewModel-klasse som håndterer alle forespørsler og tilsvarende en Repository-klasse som utfører de ulike nettverksforespørslene. Du bør også lage modellklasser som representerer de data som lastes ned. Ser du på data du får fra /users ser du at hvert user-objekt, i tillegg til enkeltverdier, inneholder et Address- og et Company-objekt. Address-objektet inneholder igjen et Geo-objekt. Lag dermed følgende klasser med attributter tilsvarende det du får fra /users: User, Address, Company, Geo, Photo, Album. Du kan dermed laste ned og konvertere  user-objektene på json-format, og konvertere til en liste med komplette User - javaobjekter som vist her (en metode i Repository-klassen): downloadUsers.txtForhåndsvis dokumentet  

NB! Et annet viktig element er å bruke et singelton-objekt til Volley-køen slik at du hele tiden opererer med en kø (og ikke flere). Siden appen vil gjøre mange forespørsler mot serveren bør/må du bruke dette. Bruk denne klassen (MySingletonQueue.javaForhåndsvis dokumentet) til dette. Eksempel på bruk, f.eks. fra Repository-klassen:

. . .
`queue = MySingletonQueue.getInstance(context).getRequestQueue();`
. . .
I tillegg vil du trenge Adapter-klasser for å vise lister med bruker, album og foto.

Ved klikk på en bruker åpner man album-aktiviteten (send med albumId via Intent) som viser alle album for aktuell bruker.

Ved klikk på album åpnes en ny aktivitet som viser en liste med (id+navn+evt. thumbnail) alle foto/bilder i dette albumet. Pass på sende med `albumId` (i Intentet som brukes til å starte aktiviteten).

Ved klikk på foto/bilde åpnes en ny aktivitet som kun viser bildet i en ImageView. Bruk også Volley til nedlasting av bilde. Eksempel på en metode som gjøre dette finner du her: volleyImage.txtForhåndsvis dokumentet (denne metoden vil f.eks. kunne ligge i Repository-klassen).

Dersom du ønsker å vise thumbnail-bildene i lista med photos kan dette gjøres vha. Volley eller andre bildebiblioteker som f.eks. Glide eller Picasso. Fra Adatapterklassen kan du f.eks. gjøre som vist her: PhotosAdapter.javaForhåndsvis dokumentet

For å kunne bruke Glide må følgende inn i gradle-fila under dependencies:
```
implementation 'com.github.bumptech.glide:glide:4.11.0'
annotationProcessor 'com.github.bumptech.glide:compiler:4.11.0'
```
 Data lastes ned vha. Volley og REST-forsepørsler.

Last ned og ta i bruk Gson-biblioteket for å konvertere JSON-stringen fra serveren til relevante objekter.

NB! Bruk anbefalt app-arkitektur ved at å ta i bruk ViewModel,  LiveData osv. som gjennomgått i timene. Pass på å lage en repository-klasse som gjør nedlasting vha. Volley. Bruk også Gson til konvertering mellom JSON og Java-objekter.
 

**TIPS:**

For å kunne bruke Lifecycle-komponenter (LiveData osv.), Volley og Gson  må følgende må inn i build.gradle, under dependencies:
```
dependencies {
    . . .
  // Volley:
  implementation 'com.android.volley:volley:1.1.1'

  // Lifecycle components
  implementation 'androidx.lifecycle:lifecycle-extensions:2.2.0'

  // Gson / Json
  implementation 'com.google.code.gson:gson:2.8.6'}
```
Dersom du bruker http (i stedet for https) må følgende inn i manifestfila:

   android:usesCleartextTraffic="true"

# Innlevering

Campusstudenter:

Levere en film med opptak av skjermen der du demonstrerer løsninga. Maks 10 minutter.

Nettstudenter:

Løsninga demonstreres og koden gjennomgås og forklares for studentassistent på avtalt tidspunkt. Inntil 10 minutter (men du har 15 min tilgjengelig). Info. om hvordan avtale nettmøte med studentassistent kommer. Det legges opp til at gjennomgangen utføres i løpet av noen dager etter innleveringsfristen. 
Nettstudent: Informasjon om presentasjoner