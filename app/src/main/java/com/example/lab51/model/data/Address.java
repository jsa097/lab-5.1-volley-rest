package com.example.lab51.model.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public @Data class Address {
	private String street;
	private String suite;
	private String city;
	private String zipcode;
	private Geo geo;
}
