package com.example.lab51.model.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public @Data class Album {
	private Long userId;
	private Long id;
	private String title;
}
