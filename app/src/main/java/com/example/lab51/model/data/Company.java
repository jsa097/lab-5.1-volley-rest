package com.example.lab51.model.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public @Data class Company {
	private String name;
	private String catchPhrase;
	private String bs;
}
