package com.example.lab51.model.data;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public @Data class Photo {
	private Long albumId;
	private Long id;
	private String title;
	private String url;
	private String thumbnailUrl;
}
