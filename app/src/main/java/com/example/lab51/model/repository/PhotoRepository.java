package com.example.lab51.model.repository;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.lab51.model.SingletonQueue;
import com.example.lab51.model.data.Photo;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

public class PhotoRepository {

	private MutableLiveData<List<Photo>> mData = new MutableLiveData<>();
	private MutableLiveData<String> mError = new MutableLiveData<>();
	private static final String Url = "https://jsonplaceholder.typicode.com/photos";
	private RequestQueue requestQueue;

	public void downloadData(Context context, long albumId)
	{
		requestQueue = SingletonQueue.getInstance(context).getRequestQueue();
		Gson gson = new Gson();
		Type collectionType = new TypeToken<List<Photo>>() {}.getType();
		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(
				Request.Method.GET,
				Url + "?albumId=" + albumId,
				null,
				response    -> mData.postValue(gson.fromJson(response.toString(), collectionType)),
				error       -> mError.postValue(error.getMessage())
		);
		requestQueue.add(jsonArrayRequest);
	}

	public MutableLiveData<List<Photo>> getData()
	{
		return mData;
	}

	public MutableLiveData<String> getError()
	{
		return mError;
	}
}
