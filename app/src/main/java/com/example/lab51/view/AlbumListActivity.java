package com.example.lab51.view;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lab51.R;
import com.example.lab51.model.data.Album;
import com.example.lab51.viewmodel.AlbumViewModel;
import com.example.lab51.viewmodel.adapter.AlbumAdapter;

import java.util.List;

public class AlbumListActivity extends AppCompatActivity {
	private RecyclerView mRecyclerView;
	private RecyclerView.Adapter mAdapter;
	private RecyclerView.LayoutManager mLayoutManager;
	private AlbumViewModel mAlbumViewModel;
	public static final String USER_ID_KEY = "USER_ID";


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_album_list);

		Bundle bundle = getIntent().getExtras();
		long id;
		if (bundle == null) {
			Toast.makeText(getApplicationContext(), "not found", Toast.LENGTH_LONG).show();
			System.out.println("id not found");
			finish();
		}
		id = bundle.getLong(USER_ID_KEY);
		Toast.makeText(getApplicationContext(), String.valueOf(id), Toast.LENGTH_LONG).show();


		mRecyclerView = findViewById(R.id.album_recycler_view);

		mLayoutManager = new LinearLayoutManager(this);
		mRecyclerView.setLayoutManager(mLayoutManager);

		final Observer<List<Album>> albumDataObserver = albumData -> {
			System.out.println("User data updated.");
			albumData.forEach(album -> System.out.println(album.getTitle()));
			mAdapter = new AlbumAdapter(albumData);
			mRecyclerView.setAdapter(mAdapter);
		};
		mAlbumViewModel = new ViewModelProvider(this).get(AlbumViewModel.class);

		mAlbumViewModel.setUserId(id);

		mAlbumViewModel.getAlbumData().observe(this, albumDataObserver);

		mAlbumViewModel.requestDownload(this);
	}
}
