package com.example.lab51.view;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.example.lab51.R;
import com.squareup.picasso.Picasso;

public class PhotoDisplayActivity extends AppCompatActivity {

	public static final String PHOTO_URL_KEY = "PHOTO_URL";

	@Override
	protected void onCreate(@Nullable Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_display);

		Bundle bundle = getIntent().getExtras();
		if (bundle == null || bundle.getString(PHOTO_URL_KEY) == null)
		{
			Toast.makeText(getApplicationContext(), "not found", Toast.LENGTH_LONG).show();
			finish();
			return;
		}
		final String URL = bundle.getString(PHOTO_URL_KEY);
		Toast.makeText(getApplicationContext(), URL + ".jpg", Toast.LENGTH_LONG).show();
		System.out.println("Downloading image: " + URL);
		Picasso.get().load(URL.replaceAll("600", "600.png")).into((ImageView) findViewById(R.id.photo_display));
	}


	}
