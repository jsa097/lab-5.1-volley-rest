package com.example.lab51.view;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lab51.R;
import com.example.lab51.model.data.Photo;
import com.example.lab51.viewmodel.PhotoViewModel;
import com.example.lab51.viewmodel.adapter.PhotoAdapter;

import java.util.List;

public class PhotoListActivity extends AppCompatActivity {
	private RecyclerView mRecyclerView;
	private RecyclerView.Adapter mAdapter;
	private RecyclerView.LayoutManager mLayoutManager;
	private PhotoViewModel mPhotoViewModel;
	public static final String ALBUM_ID_KEY = "ALBUM_ID";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_photo_list);

		Bundle bundle = getIntent().getExtras();
		long id;
		if (bundle == null) {
			Toast.makeText(getApplicationContext(), "not found", Toast.LENGTH_LONG).show();
			System.out.println("id not found");
			finish();
		}
		id = bundle.getLong(ALBUM_ID_KEY);
		Toast.makeText(getApplicationContext(), String.valueOf(id), Toast.LENGTH_LONG).show();


		mRecyclerView = findViewById(R.id.photo_recycler_view);

		mLayoutManager = new LinearLayoutManager(this);
		mRecyclerView.setLayoutManager(mLayoutManager);

		final Observer<List<Photo>> photoDataObserver = photoData -> {
			System.out.println("User data updated.");
			photoData.forEach(album -> System.out.println(album.getTitle()));
			mAdapter = new PhotoAdapter(photoData);
			mRecyclerView.setAdapter(mAdapter);
		};
		mPhotoViewModel = new ViewModelProvider(this).get(PhotoViewModel.class);

		mPhotoViewModel.setAlbumId(id);

		mPhotoViewModel.getPhotoData().observe(this, photoDataObserver);

		mPhotoViewModel.requestDownload(this);
	}
}
