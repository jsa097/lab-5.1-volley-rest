package com.example.lab51.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lab51.R;
import com.example.lab51.model.data.User;
import com.example.lab51.viewmodel.UserViewModel;
import com.example.lab51.viewmodel.adapter.UserAdapter;

import java.util.List;

public class UserListActivity extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private UserViewModel mUserViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_list);

        mRecyclerView = findViewById(R.id.user_recycler_view);

        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);

	    final Observer<List<User>> userDataObserver = userData -> {
		    System.out.println("User data updated.");
		    userData.forEach(user -> System.out.println(user.getName()));
		    mAdapter = new UserAdapter(userData);
		    mRecyclerView.setAdapter(mAdapter);
	    };
	    mUserViewModel = new ViewModelProvider(this).get(UserViewModel.class);

	    mUserViewModel.getUserData().observe(this, userDataObserver);

	    mUserViewModel.requestDownload(this);
    }
}
