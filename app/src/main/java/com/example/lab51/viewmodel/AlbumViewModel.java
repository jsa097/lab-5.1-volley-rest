package com.example.lab51.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.lab51.model.data.Album;
import com.example.lab51.model.repository.AlbumRepository;

import java.util.List;

public class AlbumViewModel extends ViewModel {
	private AlbumRepository albumRepository;
	private MutableLiveData<List<Album>> albumData;
	private long userId;

	public AlbumViewModel() {
		this.albumRepository = new AlbumRepository();
		this.albumData = this.albumRepository.getData();
	}

	public MutableLiveData<List<Album>> getAlbumData() {
		return this.albumData;
	}

	public void requestDownload(Context context) {
		this.albumRepository.downloadData(context, userId);
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}
}
