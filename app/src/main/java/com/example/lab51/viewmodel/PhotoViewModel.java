package com.example.lab51.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.lab51.model.data.Photo;
import com.example.lab51.model.repository.PhotoRepository;

import java.util.List;

public class PhotoViewModel extends ViewModel {
	private PhotoRepository photoRepository;
	private MutableLiveData<List<Photo>> photoData;
	private long albumId;

	public PhotoViewModel() {
		this.photoRepository = new PhotoRepository();
		this.photoData = this.photoRepository.getData();
	}

	public MutableLiveData<List<Photo>> getPhotoData() {
		return this.photoData;
	}

	public void requestDownload(Context context) {
		this.photoRepository.downloadData(context, albumId);
	}

	public void setAlbumId(long albumId) {
		this.albumId = albumId;
	}
}
