package com.example.lab51.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.lab51.model.data.User;
import com.example.lab51.model.repository.UserRepository;

import java.util.List;

public class UserViewModel extends ViewModel {
	private UserRepository userRepository;
	private MutableLiveData<List<User>> userData;

	public UserViewModel() {
		this.userRepository = new UserRepository();
		this.userData = this.userRepository.getData();
	}

	public MutableLiveData<List<User>> getUserData() {
		return this.userData;
	}

	public void requestDownload(Context context) {
		this.userRepository.downloadData(context);
	}
}