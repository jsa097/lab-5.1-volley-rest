package com.example.lab51.viewmodel.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lab51.R;
import com.example.lab51.model.data.Album;
import com.example.lab51.view.PhotoListActivity;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class AlbumAdapter extends RecyclerView.Adapter {
	private List<Album> mAlbumData;
	public static final String ALBUM_ID_KEY = "ALBUM_ID";

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.row_album, parent, false);

		return new AlbumAdapter.AlbumViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
		AlbumAdapter.AlbumViewHolder h = (AlbumAdapter.AlbumViewHolder) holder;
		Album album = mAlbumData.get(position);
		h.mTitleView.setText(album.getTitle());
		h.mRootView.setOnClickListener(
				v -> {
					Activity activity = (Activity) v.getContext();
					Intent intent = new Intent(activity, PhotoListActivity.class);
					intent.putExtra(ALBUM_ID_KEY, album.getId());
					activity.startActivity(intent);
				}
		);
	}

	@Override
	public int getItemCount() {
		return mAlbumData.size();
	}

	public static class AlbumViewHolder extends RecyclerView.ViewHolder {
		public TextView mIdView, mTitleView;
		public View mRootView;

		public AlbumViewHolder(@NonNull View itemView) {
			super(itemView);
			this.mIdView = itemView.findViewById(R.id.id);
			this.mTitleView = itemView.findViewById(R.id.title);
			mRootView = itemView;
		}
	}
}
