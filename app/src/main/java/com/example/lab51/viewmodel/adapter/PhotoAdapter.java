package com.example.lab51.viewmodel.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lab51.R;
import com.example.lab51.model.data.Photo;
import com.example.lab51.view.PhotoDisplayActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class PhotoAdapter extends RecyclerView.Adapter {
	private List<Photo> mPhotoData;
	public static final String PHOTO_URL_KEY = "PHOTO_URL";

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.row_photo, parent, false);

		return new PhotoAdapter.PhotoViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
		PhotoAdapter.PhotoViewHolder h = (PhotoAdapter.PhotoViewHolder) holder;
		Photo photo = mPhotoData.get(position);
		h.mIdView.setText(photo.getId().toString());
		h.mTitleView.setText(photo.getTitle());
		h.mRootView.setOnClickListener(
				v -> {
					Activity activity = (Activity) v.getContext();
					Intent intent = new Intent(activity, PhotoDisplayActivity.class);
					intent.putExtra(PHOTO_URL_KEY, photo.getUrl());
					activity.startActivity(intent);
				}
		);
		Picasso.get().load(photo.getThumbnailUrl()).into(h.mPhotoView);
	}

	@Override
	public int getItemCount() {
		return mPhotoData.size();
	}

	public static class PhotoViewHolder extends RecyclerView.ViewHolder {
		public TextView mIdView, mTitleView;
		public ImageView mPhotoView;
		public View mRootView;

		public PhotoViewHolder(@NonNull View itemView) {
			super(itemView);
			this.mIdView = itemView.findViewById(R.id.id);
			this.mTitleView = itemView.findViewById(R.id.title);
			this.mPhotoView = itemView.findViewById(R.id.thumbnail);
			this.mRootView = itemView;
		}
	}
}
