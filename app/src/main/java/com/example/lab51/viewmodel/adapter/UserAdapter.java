package com.example.lab51.viewmodel.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.lab51.R;
import com.example.lab51.model.data.User;
import com.example.lab51.view.AlbumListActivity;

import java.util.List;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class UserAdapter extends RecyclerView.Adapter {
	private List<User> mUserData;
	public static final String USER_ID_KEY = "USER_ID";

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
		View itemView = LayoutInflater.from(parent.getContext())
				.inflate(R.layout.row_user, parent, false);

		return new UserViewHolder(itemView);
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
		UserViewHolder h = (UserViewHolder) holder;
		User user = mUserData.get(position);
		h.mUsernameView.setText("@" + user.getUsername());
		h.mNameView.setText(user.getName());
		h.mRootView.setOnClickListener(
				v -> {
					Activity activity = (Activity) v.getContext();
					Intent intent = new Intent(activity, AlbumListActivity.class);
					intent.putExtra(USER_ID_KEY, user.getId());
					activity.startActivity(intent);
				}
		);
	}

	@Override
	public int getItemCount() {
		return mUserData.size();
	}

	public static class UserViewHolder extends RecyclerView.ViewHolder {
		public TextView mUsernameView, mNameView;
		public View mRootView;

		public UserViewHolder(@NonNull View itemView) {
			super(itemView);
			this.mUsernameView = itemView.findViewById(R.id.username);
			this.mNameView = itemView.findViewById(R.id.name);
			mRootView = itemView;
		}
	}
}
